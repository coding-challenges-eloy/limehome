module.exports = {
  collectCoverageFrom: ['src/**/*.js'],
  testEnvironment: 'node',
  verbose: true
};
