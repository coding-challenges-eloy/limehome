import { hotels } from './handler';

jest.mock('./lib/geo', () => ({
  getPlaces: jest.fn()
}));

test('inital test', async () => {
  expect.assertions(1);
  expect(await hotels({})).toStrictEqual(
    expect.objectContaining({ statusCode: 200 })
  );
});
