import { getPlaces } from './lib/geo';

export const hotels = async event => {
  console.log(event);
  let hereResponse = null;

  try {
    hereResponse = await getPlaces(event.queryStringParameters);
  } catch (error) {
    console.log('Error', error);
  }
  return {
    statusCode: 200,
    body: JSON.stringify(hereResponse && hereResponse.results, null, 2)
  };
};
