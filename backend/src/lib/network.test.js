import { handleError } from './network';

test('response within 200-299 status code', async () => {
  expect.assertions(1);
  expect(handleError({ ok: true })).toStrictEqual(
    expect.objectContaining({ ok: true })
  );
});

test('response outside range 200-299 status code', async () => {
  expect.assertions(1);
  try {
    handleError({ ok: false, statusText: 'Some error' });
  } catch (error) {
    expect(error).toStrictEqual(new Error('Network Error: Some error'));
  }
});
