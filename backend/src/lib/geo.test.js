import { getPlaces } from './geo';

jest.mock('./network', () => ({
  fetch: jest.fn(url => url)
}));

test('request geolocation service with given coordinates', async () => {
  expect.assertions(1);
  expect(await getPlaces({ coordinates: 'lat,lon' })).toStrictEqual(
    expect.stringContaining('lat,lon')
  );
});
