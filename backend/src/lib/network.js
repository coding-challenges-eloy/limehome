import nodeFetch from 'node-fetch';

/**
 * `fetch` will only reject on network failure or
 * if anything prevented the request from completing.
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch}
 *
 * @function handleError
 * @param  {Response} res
 * @return {Object}
 */
export const handleError = res => {
  if (res.ok) return res;
  throw new Error(`Network Error: ${res.statusText}`);
};

/**
 * Simplify fetch to return a JSON object
 * and handle not succesful response status codes
 *
 * @function fetch
 * @param {String} resource
 * @param {Object} init @see {@link https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch#Parameters}
 * @return {Object} Result of parsing the body text as JSON
 * @throws {NetworkError} Response status code not within 200 range
 * @throws {SyntaxError} Can not parse body text as JSON
 */
export const fetch = async (resource, init) =>
  nodeFetch(resource, init)
    .then(handleError)
    .then(res => res.json());
