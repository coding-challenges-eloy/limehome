import { fetch } from './network';

const HERE_APP_ID = '7iaOSlTZbbH5n7xtDNTt';
const HERE_APP_CODE = '_e9slodGDa7MltTXudWnLA';
const HERE_API_BASE_URL = 'https://places.cit.api.here.com/places/v1/browse';

export const getPlaces = async ({ coordinates, address }) => {
  let url = HERE_API_BASE_URL;
  url = `${url}?cat=accommodation&app_id=${HERE_APP_ID}&app_code=${HERE_APP_CODE}`;
  if (coordinates) {
    url = `${url}&at=${coordinates}`;
  }

  const response = await fetch(url);

  console.log('hereResponse', response);

  return response;

  // const url = `https://places.cit.api.here.com/places/v1/browse?at=${location}&cat=accommodation&app_id=${HERE_APP_ID}&app_code=${HERE_APP_CODE}`;
  // hereResponse = await fetch(url);
};
